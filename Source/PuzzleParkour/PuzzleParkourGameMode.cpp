// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "PuzzleParkourGameMode.h"
#include "PuzzleParkourCharacter.h"
#include "UObject/ConstructorHelpers.h"

APuzzleParkourGameMode::APuzzleParkourGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Character/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
