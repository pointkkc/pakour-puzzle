// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Actor_Door.h"
#include "GameFramework/Character.h"
#include "PuzzleParkourCharacter.generated.h"

UCLASS(config=Game)
class APuzzleParkourCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	APuzzleParkourCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:
	
	virtual void BeginPlay() override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }


	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
	class UCapsuleComponent* TriggerCapsule;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// declare overlap end function
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//UFUNCTION()
		//void OnComponentHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	// Weapon Mesh
	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* Weapon;

	// current door instance
	class AActor_Door* CurrentDoor = NULL;
	// current puzzle object instance
	class APuzzleObject* CurrentPuzzleObject = NULL;
	// current charge station instance
	class ARailgunChargeStation* CurrentChargeStation;

	//weapon muzzle location via UArrowComponent
	UPROPERTY(VisibleAnywhere)
	class UArrowComponent* MuzzleLocation;
	//projectile
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class ARailgunProjectile> ProjectileClass;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite,Category = "Animation")
	bool JumpButtonDown;
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Animation")
	bool CrouchButtonDown;

protected:
	void OnAction();
	void OnShoot();
	void OnDeleteSave();
	void OnDisplaySave();

public:

	//Current Ammo start zero
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Weapon")
	int Ammo = 0;
	//Max Ammo thar can carry
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Weapon")
	int MaxAmmo = 10;

	//Game Playtime Tracking
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Game")
	float PlayTime;

	//return current playtime
	float GetPlayTime();

	//Send to FireAnimation
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Game")
	bool IsFire;

	//SOUND
	UPROPERTY(EditDefaultsOnly,  Category = "FX")
	class USoundBase* WeaponSound;
	UPROPERTY(EditDefaultsOnly,  Category = "FX")
	class UParticleSystem* WeaponParticle;
	UPROPERTY(EditDefaultsOnly,  Category = "FX")
	class USoundBase* ReAmmoSound;
	UPROPERTY(EditDefaultsOnly, Category = "FX")
	class USoundBase* EndSound;

	//Call to BP for popup endgame menu
	void CallEndGame();

};

