// Fill out your copyright notice in the Description page of Project Settings.

#include "RailgunChargeStation.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ARailgunChargeStation::ARailgunChargeStation()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
	RootComponent = Scene;

	ChargeStationMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ChargeStation"));
	ChargeStationMesh->SetupAttachment(RootComponent);
	

	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerVolume"));
	TriggerVolume->InitBoxExtent(FVector(150, 100, 100));
	TriggerVolume->SetCollisionProfileName("Trigger");
	TriggerVolume->SetupAttachment(RootComponent);
	//BoxComp->AttachTo(puzzleObject, "NONE", EAtt

}



int ARailgunChargeStation::RequestCharge()
{
	UE_LOG(LogTemp, Warning, TEXT("Charging up your railgun"));
	return 1;
}


