// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RailgunChargeStation.generated.h"

UCLASS()
class PUZZLEPARKOUR_API ARailgunChargeStation : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARailgunChargeStation();

	int RequestCharge();

private:

	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* ChargeStationMesh;

	UPROPERTY(VisibleAnywhere) // make can visible/edit in bp
	class UBoxComponent* TriggerVolume;


};
