// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "PuzzleParkourCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Puzzle/PuzzleObject.h" //for interact puzzle object
#include "Components/StaticMeshComponent.h"// for weapon mesh
#include "Components/ArrowComponent.h" //muzzle
#include "Weapon/RailgunProjectile.h" //projectile
#include "Weapon/RailgunChargeStation.h" //railgun ammobox
#include "Mechanics/PuzzleGameInstance.h"// game ins
#include "Kismet/GameplayStatics.h"
#include "OutputDeviceNull.h"//for call event to bp

//////////////////////////////////////////////////////////////////////////
// APuzzleParkourCharacter

APuzzleParkourCharacter::APuzzleParkourCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.f, 96.0f);;
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);

	// bind trigger events
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &APuzzleParkourCharacter::OnOverlapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &APuzzleParkourCharacter::OnOverlapEnd);
	//weapon
	Weapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon"));
	//muzzle
	MuzzleLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("MuzzleLocation"));
	MuzzleLocation->SetupAttachment(Weapon);

	CurrentDoor = NULL;
}

//////////////////////////////////////////////////////////////////////////
// Input

void APuzzleParkourCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &APuzzleParkourCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APuzzleParkourCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &APuzzleParkourCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APuzzleParkourCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &APuzzleParkourCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &APuzzleParkourCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &APuzzleParkourCharacter::OnResetVR);

	//interact
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &APuzzleParkourCharacter::OnAction);

	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &APuzzleParkourCharacter::OnShoot);

	PlayerInputComponent->BindAction("DeleteSave", IE_Pressed, this, &APuzzleParkourCharacter::OnDeleteSave);

	PlayerInputComponent->BindAction("DisplaySave", IE_Pressed, this, &APuzzleParkourCharacter::OnDisplaySave);
}

void APuzzleParkourCharacter::OnShoot() {
	
	if (ProjectileClass != NULL) {
		UWorld* const World = GetWorld();
		if (World != NULL) {
			if(Ammo > 0){
				IsFire = true; //false set on AnimBP
				Ammo--;
				UE_LOG(LogTemp, Warning, TEXT("CurrentAmmo = %d"), Ammo);

				FRotator SpawnRotation = MuzzleLocation->GetComponentRotation();
				FVector SpawnLocation;
				if (MuzzleLocation != nullptr)
				{
					SpawnLocation = MuzzleLocation->GetComponentLocation();
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("MUZZLE = NULL"));
				}

				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				World->SpawnActor<ARailgunProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

				//Sound
				if(WeaponSound) UGameplayStatics::PlaySoundAtLocation(World, WeaponSound, MuzzleLocation->GetComponentLocation(), 1);
				//WeaponParticle
				if(WeaponParticle) UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponParticle, MuzzleLocation->GetComponentLocation(),GetActorRotation());

			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("No Ammo"));
				Ammo = 0;
			}
			
			
		}
	}
}

void APuzzleParkourCharacter::OnDeleteSave()
{
	UPuzzleGameInstance* GameIns = Cast<UPuzzleGameInstance>(GetWorld()->GetGameInstance());
	if (GameIns) {
		GameIns->DeleteSaveGame();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("can't DeleteSaveGame"));
	}
	
	
}

void APuzzleParkourCharacter::OnDisplaySave()
{
	UWorld* const World = GetWorld();
	UPuzzleGameInstance* GameIns = Cast<UPuzzleGameInstance>(World->GetGameInstance());
	if (GameIns) {
		//GameIns->DeleteSaveGame();
		UE_LOG(LogTemp, Warning, TEXT("Display Rank"));
		GameIns->DisplayRank();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No GameIns"));
	}
}

float APuzzleParkourCharacter::GetPlayTime()
{
	return PlayTime;
}

void APuzzleParkourCharacter::OnAction()
{
	FVector ForwardVector = GetFollowCamera()->GetForwardVector();
	
	if (CurrentDoor)
	{
		CurrentDoor->ToggleDoor(ForwardVector);
		
	}
	if (CurrentPuzzleObject) 
	{
		CurrentPuzzleObject->SpawnPuzzleObject();

	}
	if (CurrentChargeStation) {
		Ammo += CurrentChargeStation->RequestCharge();
		if (Ammo > MaxAmmo) {
			Ammo = MaxAmmo;
		}
		else
		{
			UWorld* const World = GetWorld();
			if (ReAmmoSound) UGameplayStatics::PlaySoundAtLocation(World, ReAmmoSound, CurrentChargeStation->GetActorLocation(), 1);
		}
		UE_LOG(LogTemp, Warning, TEXT("CAmmo = %d"), Ammo);
	}
}


void APuzzleParkourCharacter::BeginPlay()
{
	Super::BeginPlay();
	FAttachmentTransformRules Rules = FAttachmentTransformRules::KeepRelativeTransform;
	//ACharacter* thisCharacter = Cast<ACharacter>(GetOwner());
	if (Weapon != nullptr) {
			UE_LOG(LogTemp, Warning, TEXT("AttachTo Railgun_Hand_R"));
			Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "Railgun_Hand_R");
			
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Weapon = nullptr"));
	}

	
}

void APuzzleParkourCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void APuzzleParkourCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void APuzzleParkourCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void APuzzleParkourCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APuzzleParkourCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void APuzzleParkourCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void APuzzleParkourCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void APuzzleParkourCharacter::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->GetClass()->IsChildOf(AActor_Door::StaticClass()))
	{
		CurrentDoor = Cast<AActor_Door>(OtherActor);
	}
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->GetClass()->IsChildOf(APuzzleObject::StaticClass()))
	{
		CurrentPuzzleObject = Cast<APuzzleObject>(OtherActor);
	}
	// add ammo box here
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->GetClass()->IsChildOf(ARailgunChargeStation::StaticClass()))
	{
		CurrentChargeStation = Cast<ARailgunChargeStation>(OtherActor);
	}
}

// overlap on end function
void APuzzleParkourCharacter::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		CurrentDoor = NULL;
		CurrentPuzzleObject = NULL;
		CurrentChargeStation = NULL;
		
	}
}

void APuzzleParkourCharacter::CallEndGame()
{
	FOutputDeviceNull ar;
	this->CallFunctionByNameWithArguments(TEXT("Event_EndGame"), ar, NULL, true);
	UWorld* const World = GetWorld();
	if(EndSound) UGameplayStatics::PlaySoundAtLocation(World, EndSound, GetActorLocation());
	
}