// Fill out your copyright notice in the Description page of Project Settings.

#include "TriggerEndGame.h"
#include "Components/BoxComponent.h" // Use Boxcomponent 
#include "Kismet/GameplayStatics.h"
#include "PuzzleParkourCharacter.h"
#include "PuzzleGameInstance.h"
#include "SaveScore.h"


// Sets default values
ATriggerEndGame::ATriggerEndGame()
{
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume")); // add sub component to c++ blueprint
	if (!ensure(TriggerVolume != nullptr)) return;
	RootComponent = TriggerVolume; // set TriggerVolume to root 

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &ATriggerEndGame::OnOverlapBegin);

}

void ATriggerEndGame::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//EndGame
	//getworld for player ref
	UWorld* WLRD = GetWorld();
	if (WLRD == nullptr) return;

	//get playercharacter for time
	APuzzleParkourCharacter* PlayerCharacter = Cast<APuzzleParkourCharacter>(UGameplayStatics::GetPlayerCharacter(WLRD, 0));
	if (PlayerCharacter == nullptr) return;
	//get time
	float PlayTime = PlayerCharacter->GetPlayTime();
	UE_LOG(LogTemp, Warning, TEXT("NewComplete time is = %f"), PlayTime);
	
	//send completetime to gameinstance and calcurate rankings!
	UPuzzleGameInstance* GameIns = Cast<UPuzzleGameInstance>(WLRD->GetGameInstance());
	if (GameIns == nullptr) return;
	GameIns->EndGame(PlayTime);
	
	//gotomenu
}
// Called every frame


