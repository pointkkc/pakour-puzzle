// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleGameInstance.h"
#include "SaveScore.h"
#include "Kismet/GameplayStatics.h"
#include "PuzzleParkourCharacter.h"

UPuzzleGameInstance::UPuzzleGameInstance(const FObjectInitializer& ObjectInitializer) 
: Super(ObjectInitializer) 
{
	Rank.Init(2400,6); //Setup Default Array
	SetupGame();
	UE_LOG(LogTemp, Warning, TEXT("SetupGame Complete"));
}

void UPuzzleGameInstance::SetupGame()
{
	USaveScore* SaveGameInstance;
	
	//LOAD SAVE
	if (UGameplayStatics::DoesSaveGameExist(TEXT("PuzzlePakour"), 0)) {
		SaveGameInstance = Cast<USaveScore>(UGameplayStatics::LoadGameFromSlot(TEXT("PuzzlePakour"), 0));
		
		if (SaveGameInstance) {
			UE_LOG(LogTemp, Warning, TEXT("LoadGameFromSlot Complete "));
			//make float to array
			Rank = SaveGameInstance->Rank; //Set Rank from loaded rank
			AssignRanking();
			UE_LOG(LogTemp, Warning, TEXT("AssignRanking Complete"));
		}
	}
	//CREATE NEW SAVE
	else
	{
		SaveGameInstance = Cast<USaveScore>(UGameplayStatics::CreateSaveGameObject(USaveScore::StaticClass()));
		if (SaveGameInstance) {
			SaveGameInstance->Rank = Rank;
			AssignRanking();
			SaveGame();
			UE_LOG(LogTemp, Warning, TEXT("Create new Save Complete "));
		} else {
			UE_LOG(LogTemp, Warning, TEXT("SaveScore does not exist "));
		}
	}
}

void UPuzzleGameInstance::DeleteSaveGame()
{
	if (UGameplayStatics::DeleteGameInSlot(TEXT("PuzzlePakour"), 0)) {
		UE_LOG(LogTemp, Warning, TEXT("Delete Complete "));
	}else{
		UE_LOG(LogTemp, Warning, TEXT("No GameInSlot PuzzlePakour"));
	}	
}

void UPuzzleGameInstance::SaveGame()
{
	USaveScore* SaveGameInstance = Cast<USaveScore>(UGameplayStatics::CreateSaveGameObject(USaveScore::StaticClass()));
	SaveGameInstance->Rank = Rank;
	SaveGameInstance->DisplayRankArray();
	
	if (UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("PuzzlePakour"), 0)) {
		UE_LOG(LogTemp, Warning, TEXT("Save Complete "));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Save Not Complete "));
	}

}

void UPuzzleGameInstance::DisplayRank()
{
	DisplayRankArray();
}

void UPuzzleGameInstance::EndGame(float NewTime)
{
	//call if checkpoint pass 15 times
	if (CheckpointPassed == 16) {
		CalculateRankings(NewTime);
		SaveGame();
		UWorld* WLRD = GetWorld();
		if (WLRD) {
			APlayerController* PlayerController = GetFirstLocalPlayerController();
			if (PlayerController) {
				APuzzleParkourCharacter* PlayerChar = Cast<APuzzleParkourCharacter>(PlayerController->GetCharacter());
				if (PlayerChar) {
					PlayerChar->CallEndGame();
					UE_LOG(LogTemp, Warning, TEXT("CallEndGame "));
					
				}
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("PlayerController null "));
			}
			
		}
		
	}
		

}



/////////////////// SAVE MECHANICS //////////////////////////

TArray<float> UPuzzleGameInstance::GetRank()
{
	return Rank;
}

void UPuzzleGameInstance::DisplayRankArray()
{
	UE_LOG(LogTemp, Warning, TEXT("Array Rank Length = %d"), Rank.Num());
	for (int i = 0; i < Rank.Num(); i++)
	{
		UE_LOG(LogTemp, Warning, TEXT("Display Rank %d complete time is %f"), i + 1, Rank[i]);
	}
	//UE_LOG(LogTemp, Warning, TEXT("complete time is %f"), FastestClearTime);

}

void UPuzzleGameInstance::CalculateRankings(float NewTime)
{
	UE_LOG(LogTemp, Warning, TEXT("Calculating Ranks NewTime = %f"), NewTime);
	//if rank array has length.
	if (Rank.Num() > 0) {
		float Max = Rank[Rank.Num() - 1];//max time complete remain last array slot
		for (int i = 0; i < Rank.Num(); i++)
		{
			// if Max time complete less than current loop time complete. 
			if (Max < Rank[i]) {
				//then insert current loop time complete to Max variable.
				Max = Rank[i];

			}
		}
		//New complete time compare with Max time complete ranking.
		if (NewTime < Max) { //if less than max.
			//insert to ranking in max time complete 
			Rank[Rank.Num() - 1] = NewTime;
			AssignRanking();

		}
		UE_LOG(LogTemp, Warning, TEXT("Max time complete value is = %f"), Max);
	}


}

//Sort Rank
void UPuzzleGameInstance::AssignRanking()
{
	//insert to ranking in max time complete 
	int i, j;
	float temp; //temp swap variable
	//sort ranking
	for (i = 0; i < Rank.Num(); i++) {// i is ranking min to max
		for (j = (i + 1); j < Rank.Num(); j++) {

			if (Rank[i] >= Rank[j]) {
				temp = Rank[i];
				Rank[i] = Rank[j]; //min time of based on i var
				Rank[j] = temp;

			}
		}
		//UE_LOG(LogTemp, Warning, TEXT("Ranks %d Complete time is %f !"), i + 1, Rank[i]);
		UE_LOG(LogTemp, Warning, TEXT("Set Rank %d complete time is %f"), i + 1, temp);


	}

}



