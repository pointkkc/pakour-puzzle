// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "SaveScore.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPARKOUR_API USaveScore : public USaveGame
{
	GENERATED_BODY()

	USaveScore();

public:
	
	//Array Rank TOO: max them can use
	UPROPERTY(EditAnyWhere)
	TArray<float> Rank;

	void DisplayRankArray();

};
