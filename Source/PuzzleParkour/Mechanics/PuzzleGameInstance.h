// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "PuzzleGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPARKOUR_API UPuzzleGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UPuzzleGameInstance(const FObjectInitializer& ObjectInitializer);

	//Setting up game
	void SetupGame();
	//Delete current save game
	void DeleteSaveGame();
	//Save game
	void SaveGame();
	//display all rank at anytime
	void DisplayRank();
	//call endgame 
	void EndGame(float NewTime);

	//Checkpoint
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Game")
	int CheckpointPassed = 1;

	// SAVE MECHANICS
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Game")
	TArray<float> Rank;
	// Return current rank
	TArray<float> GetRank();
	//Display Rank
	void DisplayRankArray();
	// Sort Ranking less time will be first array
	void CalculateRankings(float NewTime);
	// Assign Ranking if New Complete time less than 6th Rank
	void AssignRanking();
	// Call End game 
	void CallEndGame();

private:

};
