// Fill out your copyright notice in the Description page of Project Settings.

#include "SaveScore.h"
#include "PuzzleGameInstance.h"

USaveScore::USaveScore() {
	// set dafault array prevent no array slot crash
	Rank.SetNum(6);
}
// display rank array from save
void USaveScore::DisplayRankArray()
{
	UE_LOG(LogTemp, Warning, TEXT(" Array Rank Length = %d"), Rank.Num());
	for (int i = 0; i < Rank.Num(); i++)
	{
		UE_LOG(LogTemp, Warning, TEXT("INSAVE Display Rank %d complete time is %f"), i + 1, Rank[i]);
	}
	//UE_LOG(LogTemp, Warning, TEXT("complete time is %f"), FastestClearTime);

}
