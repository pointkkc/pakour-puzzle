// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleObject.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
APuzzleObject::APuzzleObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
	RootComponent = Scene;
	
	puzzleObject = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("puzzleObject"));
	puzzleObject->SetupAttachment(RootComponent);
	
	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("ResetTriggerBox"));
	BoxComp->InitBoxExtent(FVector(150, 100, 100));
	BoxComp->SetCollisionProfileName("Trigger");
	BoxComp->SetupAttachment(RootComponent);
	//BoxComp->AttachTo(puzzleObject, "NONE", EAttachLocation::Type::SnapToTarget, EAttachLocation::SnapToTarget);

}

// Called when the game starts or when spawned
void APuzzleObject::BeginPlay()
{
	Super::BeginPlay();
	puzzleObject->SetSimulatePhysics(true);
	//for reset position
	OriginLocation = puzzleObject->GetComponentLocation();
	OriginRotator = puzzleObject->GetComponentRotation();
}

void APuzzleObject::SpawnPuzzleObject()
{

	puzzleObject->SetRelativeLocation(OriginLocation);
	puzzleObject->SetRelativeRotation(OriginRotator);
	UWorld* const World = GetWorld();
	if (ResetSound) UGameplayStatics::PlaySoundAtLocation(World, ResetSound, OriginLocation, 1);
	//remove velocity and set physic back
	puzzleObject->SetSimulatePhysics(false);
	puzzleObject->SetSimulatePhysics(true);

}

