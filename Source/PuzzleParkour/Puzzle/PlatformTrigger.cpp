// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformTrigger.h"
#include "Components/BoxComponent.h" // Use Boxcomponent 

#include "MovingObject.h"

// Sets default values
APlatformTrigger::APlatformTrigger()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume")); // add sub component to c++ blueprint
	if (!ensure(TriggerVolume != nullptr)) return;
	RootComponent = TriggerVolume; // set TriggerVolume to root 

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &APlatformTrigger::OnOverlapBegin);

}


void APlatformTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Activated"));
	
	for (AMovingObject* Platform : PlatformsToTrigger) {
		if (Platform) {
			Platform->AddActiveTrigger();
		}
		
	}
}



