#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MovingObject.generated.h"

/**
*
*/

UCLASS()
class PUZZLEPARKOUR_API AMovingObject : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	AMovingObject();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	//Speed Movement of Moving Object
	UPROPERTY(EditAnywhere)
	float Speed = 20;

	//The Target for Moving to
	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
	FVector TargetLocation;
	//Set moving object move
	void AddActiveTrigger();
	//Set moving object stop
	void RemoveActiveTrigger();

private:
	FVector GlobalTargetLocation;
	FVector GlobalStartLocation;

	UPROPERTY(EditAnywhere)
		int ActiveTriggers = 1;

};
