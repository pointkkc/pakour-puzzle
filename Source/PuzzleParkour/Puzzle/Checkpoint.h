// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Checkpoint.generated.h"


UCLASS()
class PUZZLEPARKOUR_API ACheckpoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACheckpoint();
	//insert this Checkpoint Position 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
	int CheckpointPosition;

	UPROPERTY(EditAnywhere)
	class APuzzleParkourCharacter* Player;

protected:


private:	

	UPROPERTY(VisibleAnywhere) // make can visible/edit in bp
	class UBoxComponent* TriggerVolume;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
	class USoundBase* CheckpointSound;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
