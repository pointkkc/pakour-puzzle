// Fill out your copyright notice in the Description page of Project Settings.

#include "MovingObject.h"



AMovingObject::AMovingObject()
{
	PrimaryActorTick.bCanEverTick = true;
	ActiveTriggers = 0;
	SetMobility(EComponentMobility::Movable);
}


void AMovingObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (ActiveTriggers > 0)
	{
		if (HasAuthority()) { // Have Authority to move just on Server / server always right
			FVector Location = GetActorLocation();
			float JourneyLength = (GlobalTargetLocation - GlobalStartLocation).Size();
			float JourneyTravelled = (Location - GlobalStartLocation).Size();

			if (JourneyTravelled >= JourneyLength)
			{
				FVector Swap = GlobalStartLocation;
				GlobalStartLocation = GlobalTargetLocation;
				GlobalTargetLocation = Swap;
			}

			FVector Direction = (GlobalTargetLocation - GlobalStartLocation).GetSafeNormal();
			Location += Speed * DeltaTime * Direction;
			SetActorLocation(Location);
		}
	}

}

void AMovingObject::BeginPlay() {
	Super::BeginPlay();

	if (HasAuthority()) {
		SetReplicates(true); // set client to replicate cube from server
		SetReplicateMovement(true); // also can movable
	}

	GlobalStartLocation = GetActorLocation();
	GlobalTargetLocation = GetTransform().TransformPosition(TargetLocation);

}


void AMovingObject::AddActiveTrigger() {
	ActiveTriggers++;
}

void AMovingObject::RemoveActiveTrigger() {
	if (ActiveTriggers > 0) {
		ActiveTriggers = 0;
	}
}

