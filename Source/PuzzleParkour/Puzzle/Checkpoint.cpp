// Fill out your copyright notice in the Description page of Project Settings.

#include "Checkpoint.h"
#include "Components/BoxComponent.h" // Use Boxcomponent 
#include "Kismet/GameplayStatics.h"
#include "Mechanics/PuzzleGameInstance.h"
#include "PuzzleParkourCharacter.h"

// Sets default values
ACheckpoint::ACheckpoint()
{
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume")); // add sub component to c++ blueprint
	if (!ensure(TriggerVolume != nullptr)) return;
	RootComponent = TriggerVolume; // set TriggerVolume to root 

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &ACheckpoint::OnOverlapBegin);
	UE_LOG(LogTemp, Warning, TEXT("Checkpoint = %d from  %s"), CheckpointPosition,*this->GetFName().ToString());
}


void ACheckpoint::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//ThirdPersonCharacter_C_0
	Player = Cast<APuzzleParkourCharacter>(OtherActor);

	if (Player) {
		UWorld* const World = GetWorld();
		if (World == nullptr) return;
		UPuzzleGameInstance* GameIns = Cast<UPuzzleGameInstance>(World->GetGameInstance());
		if (GameIns == nullptr) return;

		if (CheckpointPosition == GameIns->CheckpointPassed) {
			UE_LOG(LogTemp, Warning, TEXT("Checkpoint %d Passed!"), GameIns->CheckpointPassed);
			GameIns->CheckpointPassed++;
			UGameplayStatics::PlaySound2D(World, CheckpointSound, 1);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Checkpoint Wrong!!?"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Overlap Wrong!!? %s"),*OtherActor->GetName());
		
	}
	
}

