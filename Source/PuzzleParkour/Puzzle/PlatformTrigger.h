// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlatformTrigger.generated.h"

UCLASS()
class PUZZLEPARKOUR_API APlatformTrigger : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APlatformTrigger();

private:

	UPROPERTY(VisibleAnywhere) // make can visible/edit in bp
	class UBoxComponent* TriggerVolume;

	UPROPERTY(EditAnywhere)//enable disable platform ez for designer
		TArray<class AMovingObject*> PlatformsToTrigger;

	// add OnOverlapBegin Event!
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
