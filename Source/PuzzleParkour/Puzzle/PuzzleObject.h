// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "PuzzleObject.generated.h"

UCLASS()
class PUZZLEPARKOUR_API APuzzleObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APuzzleObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	
	UPROPERTY(EditAnywhere)
	TSubclassOf<class APuzzleObject> ToSpawn;

	UFUNCTION(BlueprintCallable)
	void SpawnPuzzleObject();

	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* BoxComp;

	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* puzzleObject;

	FVector OriginLocation;
	FRotator OriginRotator;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
	class USoundBase* ResetSound;
};
